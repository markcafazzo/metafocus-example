var chokidar  = require('chokidar'),
childp 		    = require('child_process'),
electron      = require('electron-prebuilt'),
server       = require('electron-connect').server.create();

var watcher = chokidar.watch('.', {ignored: ['.git', '.vscode', 'node_modules']}).on('all', (event, path) => {
  console.log(event, path);
});

watcher.on('change', function(path, stats){
  server.reload();
  if (stats) {
    console.log('File', path, 'changed size to', stats.size);
  }
});
var watchedPaths = watcher.getWatched();
console.log('paths watched: ', watchedPaths);

var processWatcher = chokidar.watch('process/pid.txt');
processWatcher.on('add', path => {
	console.log(`File ${path} has changed, terminating process...`);
});

server.on('closed', function(){
	console.log('client CLOSED!');
	server.stop();
	server = null;

	while(server !== null){
		console.log('server alive!');
	}
	process.exit();
});

server.start();