var path = require('path');
var decache = require('decache');
var metafocus = require('metafocus');
decache('metafocus');
var handleFile =  metafocus.handleFile,
startReading = metafocus.startReading,
getTextFromPDF = metafocus.getTextFromPDF,
setWPM = metafocus.setWPM,
setPause = metafocus.setPause;
setFF = metafocus.setFF;
setRewind = metafocus.setRewind;

var AbsolutePathToApp = path.dirname(process.mainModule.filename);
var pathToPdftotext = AbsolutePathToApp + "/binaries/pdftotext.exe";
var pathToPdffonts = AbsolutePathToApp + "/binaries/pdffonts.exe";

var startButton = document.getElementById('start');
var pauseButton = document.getElementById('pause');
var ffButton = document.getElementById('ff');
var rewindButton = document.getElementById('rewind');
var counterDiv = document.getElementById('counter');
var wordCountDiv = document.getElementById('wordcount');
var fileNamePlaceholder = document.getElementById('fileNamePlaceholder');
var filename = '';
//console.log("fileName - found element: %o", fileNamePlaceholder);

function createReticle(){
  var template = document.querySelector('#reticle-template');
  var importedReticle = template.import.querySelector('#reticle');
  var clone = document.importNode(importedReticle.content, true);
  return clone;
}

//reticle init:
var reticleTemplate = createReticle();
var reticlePlaceholder = document.querySelector('#reticlePlaceholder');
reticlePlaceholder.appendChild(reticleTemplate);
var reticle = document.getElementById('reticle');

function simpleFilter(value){
  var matchesFilter = false;
  switch(value){
    case '<mark.cafazzo@gmail.com>,':
      matchesFilter = true;
      break;
    case '':
      matchesFilter = true;
      break;
    case '.':
      matchesFilter = true;
      break;
    case 'Mark':
      matchesFilter = true;
      break;
    case 'paizo.com,':
      matchesFilter = true;
    case 'Mar':
      matchesFilter = true;
      break;
    case '1':
      matchesFilter = true;
      break;
    case '14':
      matchesFilter = true;
      break;
    case '2,':
      matchesFilter = true;
      break;
    case 'Cafazzo':
      matchesFilter = true;
      break;
  }

  var regexNumbers = /(\d)\W+/g;
  if(value.match(regexNumbers)){
    matchesFilter = true;
  }

  return !matchesFilter;
}

function getMiddle(s) {
  if(s.length < 3) return { val: s, index: 0 };
  return s.length % 2 ? { val: s.substr(s.length / 2, 1), index: s.length / 2} : { val: s.substr((s.length / 2) - 1, 1), index: (s.length / 2) - 1};
}

startButton.addEventListener('click', function (event) {
  startReading(reticle, counterDiv);
});


pauseButton.addEventListener('click', function (event) {
  setPause(event.target);
});

ffButton.addEventListener('click', function (event) {
  setFF(event.target);
});

rewindButton.addEventListener('click', function (event) {
  setRewind(event.target);
});

var readFile = function(files){
    var filename = handleFile(files, pathToPdftotext, pathToPdffonts, simpleFilter, function(filename, wordCount){
        fileNamePlaceholder.innerHTML = "<div id='fileName' class='fileUpload showBorder'>" + filename + "</div>";
        wordCountDiv.innerHTML = wordCount;
    });
}