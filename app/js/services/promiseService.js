'use strict';
metafocus.factory('promiseService', ['$q', function($q){
  return {
    worker: function(value){
      var deferred = $q.defer();
      setTimeout(function(){
        deferred.notify('Beginning work...');
        if (value === "pdf"){
          deferred.resolve('uploaded');
        } else {
          deferred.reject('file is not allowed');
        }
      }, 2000);
      return deferred.promise;
    }
  }
}]);
