'user strict';

metafocus.directive('fileChanged', function(){
	console.log('fileChanged directive...');
	return {
		restrict:'A',
		link: function (scope, element, attrs){
			var fileChangedHandler = scope.$eval(attrs.fileChanged);
			element.bind('change', fileChangedHandler);
		}
	}
});