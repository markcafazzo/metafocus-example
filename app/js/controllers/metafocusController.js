'use strict';
var pdftext = require('pdf-textstring');
var path = require('path');
var AbsolutePathToApp = path.dirname(process.mainModule.filename);
var pathToPdftotext = AbsolutePathToApp + "/binaries/pdftotext.exe";
var pathToPdffonts = AbsolutePathToApp + "/binaries/pdffonts.exe";

metafocus.controller('metafocusController', function($scope, $log, $timeout, promiseService){
	$scope.appTitle = 'metafocus';
  $scope.serviceResult = '';
  $scope.fileText = '';
  $scope.metafocusText = '';
  $scope.tempArray = [];
  $scope.metafocusArray = [];

  $scope.counter = 0;
  $scope.max = 0;

	$scope.handleFile = function (files) {
    $scope.filePath = files[0].path;
    $scope.fileName = files[0].name;
    $scope.fileSelected = true;
    $scope.$apply();
    $scope.getTextFromPDF();
	};

  $scope.getTextFromPDF = function() {
    //console.log($scope.filePath);
    pdftext.setBinaryPath_PdfToText(pathToPdftotext);
    pdftext.setBinaryPath_PdfFont(pathToPdffonts);
    pdftext.pdftotext($scope.filePath, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        //$scope.fileText = data;
        $scope.tempArray = data.split(' ');
      }
    });
  };

  $scope.loadText = function(){
    for (var i = 0; i < $scope.tempArray.length; i++){
      var val = $scope.tempArray[i];
      if (val.length > 0) {
        $scope.metafocusArray.push({id: i, textValue: $scope.tempArray[i]});
      }
    }

    $timeout(function(){
      $scope.$apply();
    });
  };

  $scope.startReading = function(){
    for (var i_2 = 0; i_2 < $scope.metafocusArray.length; i_2++){
      (function theLoop (i) {
        alert('hi');
        setTimeout(function () {
          if (--i) {          // If i > 0, keep going
            theLoop(i);       // Call the loop again, and pass it the current value of i
          }
        }, 3000);
      })(10);

      $scope.metafocusText = $scope.metafocusArray[i_2].textValue;
    }
  };

  $scope.emptyStringFilter = function(element){
    //console.log('emptyStringFilter: ' + JSON.stringify(element));
    var regexEmptyString = "^\\s+$";
    var regexNumbers = "/^\d+$/";
    var emptyString = element.textValue.match(regexEmptyString);
    var isNumber = element.textValue.match(regexNumbers);

    if (emptyString
      //|| isNumber
      || $scope.isNumber(element.textValue)
      || element.textValue.includes('1')
      || element.textValue === "Mark"
      || element.textValue === "Mark"
      || element.textValue === "Mar"
      || element.textValue === "2,"
      || element.textValue.includes("6677096")
      || element.textValue.includes("455")
      || element.textValue.includes("mark.cafazzo@gmail.com")
      || element.textValue === "Cafazzo"
      || element.textValue.includes("paizo.com")) {
      return false;
    } else {
      return true;
    }
  };

  $scope.isNumber = function (n) {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
  };

  var callComplete = function(result){
    $log.log('callComplete');
    $scope.serviceResult = result;
  };

  var callFailed = function(reason){
    $log.log('callFailed');
    $scope.serviceResult = reason;
  };

  var logCall = function(){
    $log.log("Service call completed!");
  };

  //promiseService.worker("pd")
  //  .then(callComplete, callFailed)
  //  .finally(logCall);
});