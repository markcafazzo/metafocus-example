var metafocus = (function () {
  function startReading() {
    wait(delay).then(function () {
      reticle.innerHTML = filteredArray[counter];
      if (pause) {
        return;
      }
      if(skip) {
        counter = counter + 40;
      }
      if(rewind) {
        counter = counter - 40;
      }
      counter++;
      startReading();
    });
  }

  function handleFile(files) {
    counter = 0;
    console.log('handleFile()!');
    fileNamePlaceholder.innerHTML = "<div id='fileName' class='fileUpload showBorder'>" + files[0].name + "</div>";
    getTextFromPDF(files[0].path);
  }

  //formula:   60 / ((total_time / 1000) / total_characters) / 5 + .05
  function setWPM(wpm){
    switch(wpm){
      case '50':
        delay = 1200;
        break;
      case '100':
        delay = 600;
        break;
      case '150':
        delay = 550;
        break;
      case '200':
        delay = 450;
        break;
      case '250':
        delay = 400;
        break;
      case '400':
        delay = 250;
        break;
    }
    var wpmCalc = 60 / ((delay / 1000) / wpm) / 5 + .05;
    console.log('set WPM to: ' + wpm + "wpm - " + delay + "ms");
    console.log('wpm calc: ', wpmCalc / 10);
  };

  function setPause(){
    pause = !pause;
    if(pause) {
      pauseButton.innerHTML = "resume";
    } else {
      pauseButton.innerHTML = "pause"
      startReading();
    }
  };

  function setFF(){
    skip = !skip;
    if(skip) {
      ffButton.innerHTML = "FF";
    } else {
      ffButton.innerHTML = ">>";
    }
  };

  function setRewind(){
    rewind = !rewind;
    if(rewind) {
      rewindButton.innerHTML = "RR";
    } else {
      rewindButton.innerHTML = "<<";
    }
  };
}());

var pdftext = require('pdf-textstring');
var path = require('path');

var AbsolutePathToApp = path.dirname(process.mainModule.filename);
var pathToPdftotext = AbsolutePathToApp + "/binaries/pdftotext.exe";
var pathToPdffonts = AbsolutePathToApp + "/binaries/pdffonts.exe";
var tempArray = [];
var filteredArray = [];
var counter = 0;
var promiseCount = 0;
var delay = 700;
var pause, skip, rewind = false;
var pauseButton = document.getElementById('pause');
var ffButton = document.getElementById('ff');
var rewindButton = document.getElementById('rewind');
var fileNamePlaceholder = document.getElementById('fileNamePlaceholder');
console.log("fileName - found element: %o", fileNamePlaceholder);

function createReticle(){
  var template = document.querySelector('#reticle-template');
  var importedReticle = template.import.querySelector('#reticle');
  var clone = document.importNode(importedReticle.content, true);
  //var reticlePlaceholder = document.querySelector('#reticlePlaceholder');
  //clone.querySelector('#reader').textContent = 'reticle';
  return clone;
}

//init:
var reticleTemplate = createReticle();
var reticlePlaceholder = document.querySelector('#reticlePlaceholder');
reticlePlaceholder.appendChild(reticleTemplate);
var reticle = document.getElementById('reticle');

function simpleFilter(value){
  var matchesFilter = false;
  switch(value){
    case '<mark.cafazzo@gmail.com>,':
      matchesFilter = true;
      break;
    case '':
      matchesFilter = true;
      break;
    case '.':
      matchesFilter = true;
      break;
    case 'Mark':
      matchesFilter = true;
      break;
    case 'paizo.com,':
      matchesFilter = true;
    case 'Mar':
      matchesFilter = true;
      break;
    case '1':
      matchesFilter = true;
      break;
    case '14':
      matchesFilter = true;
      break;
    case '2,':
      matchesFilter = true;
      break;
    case 'Cafazzo':
      matchesFilter = true;
      break;
  }

  var regexNumbers = /(\d)\W+/g;
  if(value.match(regexNumbers)){
    matchesFilter = true;
  }

  return !matchesFilter;
}

function getMiddle(s) {
  if(s.length < 3) return { val: s, index: 0 };
  return s.length % 2 ? { val: s.substr(s.length / 2, 1), index: s.length / 2} : { val: s.substr((s.length / 2) - 1, 1), index: (s.length / 2) - 1};
}

function getTextFromPDF(path) {
  pdftext.setBinaryPath_PdfToText(pathToPdftotext);
  pdftext.setBinaryPath_PdfFont(pathToPdffonts);
  pdftext.pdftotext(path, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      tempArray = data.split(' ');
      filteredArray = tempArray.filter(simpleFilter);
      filteredArray.forEach(function(value, idx) {
        //console.log(value);
        //var theString = value.toString();
        var regExLineBreaks = /\r{1}/g;
        if(value.match(regExLineBreaks)) {
          var arrayOfNewWords = [];
          if((match = regExLineBreaks.exec(value)) != null) {
            //console.log('FOUND MATCH: %s AT INDEX: %d', value, match.index);
            if(match.index > 1) {
              firstWord = value.substr(0, match.index);
              extraWord = value.substr(match.index, value.length);
              filteredArray[idx] = firstWord;
              filteredArray.splice(idx + 1, 0, extraWord);
            }
          }
        }
        // var theWord = filteredArray[idx];
        // var midPoint = getMiddle(theWord);
        // var formatted = '';
        // if(midPoint.index > 0) {
        //   formatted = theWord.substr(0, midPoint.index) + '<b>' + midPoint.val + '</b>' 
        // + theWord.substr(midPoint.index + 1, theWord.length);
        // filteredArray[idx] = formatted;
        // } else {
        //   formatted = midPoint.val;
        // }
      });

      //console.log(JSON.stringify(filteredArray));
      console.log('Loaded PDF.');
    }
  });
}

function wait(delay){
  var thisPromiseCount = ++promiseCount;
  var promise = new Promise(
    function(resolve, reject){
      setTimeout(function(){
        resolve(thisPromiseCount);
      }, delay)
    }
  );
  return promise;
}

var div = document.getElementsByClassName('pause')[0];
div.addEventListener('click', function (event) {
  setPause();
});

var div = document.getElementsByClassName('ff')[0];
div.addEventListener('click', function (event) {
  setFF();
});

var div = document.getElementsByClassName('rewind')[0];
div.addEventListener('click', function (event) {
  setRewind();
});



