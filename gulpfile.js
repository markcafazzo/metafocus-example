var gulp      = require('gulp'),
childp 		    = require('child_process'),
electron      = require('electron-prebuilt'),
connect       = require('electron-connect').server.create();

// var watcher = chokidar.watch('file, dir, or glob', {
//   ignored: /[/\\]\./, persistent: true
// });
// var log = console.log.bind(console);

// watcher.on('change', function(path, stats){
//   if (stats) {
//     console.log('File', path, 'changed size to', stats.size);
//   }
// });
// watcher.close();


gulp.task('watch', function() {
  //livereload.listen();
  connect.start();
  gulp.watch('main.js', connect.restart);
  gulp.watch(['index.html'], connect.reload);
});

gulp.task('sass', function(){
  console.log('sass - generating css...');
});

gulp.task('run', ['watch', 'sass'], function(){
  //childp.spawn(electron, ['--debug=5858', './'], {stdio: 'inherit'});
});

